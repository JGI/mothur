FROM debian:jessie
LABEL Maintainer Shijie Yao, syao@lbl.gov

# make alias active
RUN sed -i 's/# alias/alias/' ~/.bashrc

# create download and move into it
WORKDIR /workdir

# download and install mothur 
RUN apt-get update && apt-get install mothur --yes

CMD mothur
