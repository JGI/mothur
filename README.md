# mothur docker image #

mothur is an open source software package for bioinformatics data processing.[1] The package is frequently used in the analysis of DNA from uncultured microbes. 
mothur is capable of processing data generated from several DNA sequencing methods including 454 pyrosequencing, Illumina HiSeq and MiSeq, Sanger, PacBio, and IonTorrent.
The image contains mothur v.1.33.3 that was last updated by 3/20/2014.
mothur is the default executable command in the image.
